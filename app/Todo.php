<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    public $fillable = [
        'name', 'state', 'user_id',
    ];

    protected $table = "todos_tbl";
}
